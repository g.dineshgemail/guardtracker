import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlertComponent } from './components/alert/alert.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ReportsComponent } from './components/reports/reports.component';
import { SiteComponent } from './components/site/site.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import {MatTableDataSource} from '@angular/material/table';

const routes: Routes = [
  {
    path:'',
    component:WrapperComponent,
    children:[
      {
        path:'dashboard',//
        component:DashboardComponent
      },
      {
        path:'reports',//
        component:ReportsComponent
      },
      {
        path:'alerts',//
        component:AlertComponent
      },
      {
        path:'site',//
        component:SiteComponent
      }
    ]
  },
  {
    path:'**',
    redirectTo:'/dashboard',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
