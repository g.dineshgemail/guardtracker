import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';

/*Summary Report*/
export interface PeriodicElement {
  sites: string;
  scantime: string;
  image: string;
  guardname: string;
  comments: string;
  guardcaptured: string;
}
/*Summary Report*/
const SUMMARY_DATA: PeriodicElement[] = [
  {sites: '1.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.berkeleyside.org/wp-content/uploads/2018/03/jacobo.jpg', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: 'https://media.istockphoto.com/photos/man-in-the-hood-picture-id150519390?k=20&m=150519390&s=612x612&w=0&h=JPcdg5jytJD7MFpAMXw5P51tCrQXFysZLkeRGVLFtxY='},
  {sites: '2.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://images.jpost.com/image/upload/f_auto,fl_lossy/t_JD_ArticleMainImageFaceDetect/471230', guardname: 'Job', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '3.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.aljazeera.com/wp-content/uploads/2019/09/be5c5c9a08ea4be5940d4bed638bdc59_18.jpeg?resize=770%2C513', guardname: 'Paul', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '4.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.berkeleyside.org/wp-content/uploads/2018/03/jacobo.jpg', guardname: 'Max', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '5.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.berkeleyside.org/wp-content/uploads/2018/03/jacobo.jpg', guardname: 'Larry', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '6.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://images.jpost.com/image/upload/f_auto,fl_lossy/t_JD_ArticleMainImageFaceDetect/471230', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '7.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.aljazeera.com/wp-content/uploads/2019/09/be5c5c9a08ea4be5940d4bed638bdc59_18.jpeg?resize=770%2C513', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '8.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.berkeleyside.org/wp-content/uploads/2018/03/jacobo.jpg', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '9.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://images.jpost.com/image/upload/f_auto,fl_lossy/t_JD_ArticleMainImageFaceDetect/471230', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: 'https://media.istockphoto.com/photos/man-in-the-hood-picture-id150519390?k=20&m=150519390&s=612x612&w=0&h=JPcdg5jytJD7MFpAMXw5P51tCrQXFysZLkeRGVLFtxY='},
  {sites: '10.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.aljazeera.com/wp-content/uploads/2019/09/be5c5c9a08ea4be5940d4bed638bdc59_18.jpeg?resize=770%2C513', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '11.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.berkeleyside.org/wp-content/uploads/2018/03/jacobo.jpg', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '12.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.berkeleyside.org/wp-content/uploads/2018/03/jacobo.jpg', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '13.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://images.jpost.com/image/upload/f_auto,fl_lossy/t_JD_ArticleMainImageFaceDetect/471230', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''},
  {sites: '14.Polo Sites', scantime: '01-10-2021 07:04:19', image: 'https://www.aljazeera.com/wp-content/uploads/2019/09/be5c5c9a08ea4be5940d4bed638bdc59_18.jpeg?resize=770%2C513', guardname: 'Steve', comments: 'Low - Safe', guardcaptured: ''}
];

/*Automatic Report */
export interface AutomaticElement {
  sitename: string;
  emailids: string;
  frequency: string;
  condition: string;
  once: boolean;
}
/*Automatic Report*/
const Automatic_DATA: AutomaticElement[] = [
  {sitename: 'Applw Chips Unit', emailids: 'tomsebastiantom@apple.com', frequency: 'shift_close', condition: '', once:true},
  {sitename: 'Applw Chips Unit', emailids: 'tomsebastiantom@apple.com', frequency: 'at 8:00 GMT', condition: '', once:true},
  {sitename: 'Applw Chips Unit', emailids: 'tomsebastiantom@apple.com', frequency: 'between 02-10-2021 00:00:00 and 02-10-2021 22:00:00 GMT', condition: '', once:true},
  {sitename: 'Applw Chips Unit', emailids: 'tomsebastiantom@apple.com', frequency: 'shift_close', condition: '', once:true},
  {sitename: 'Applw Chips Unit', emailids: 'tomsebastiantom@apple.com', frequency: 'shift_close', condition: '', once:true},
  {sitename: 'Applw Chips Unit', emailids: 'tomsebastiantom@apple.com', frequency: 'shift_close', condition: '', once:true},
  {sitename: 'Applw Chips Unit', emailids: 'tomsebastiantom@apple.com', frequency: 'shift_close', condition: '', once:true}
];
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent {
  /**Summary Report */
  displayedColumns: string[] = ['sites', 'scantime', 'image', 'guardname','comments', 'guardcaptured'];
  summarydataSource = SUMMARY_DATA;

  /**Automatic Report */
  displayedColumnsAutomatic: string[] = ['sitename', 'emailids', 'frequency', 'condition', 'once', 'actions'];
  automaticdataSource = Automatic_DATA;

  //Dropdown fir site
  myControl = new FormControl();
  siteList: string[] = ['site 1', 'Site 2', 'Site 3','site 11', 'Site 12', 'Site 13','site 21', 'Site 22', 'Site 23','site 31', 'Site 32', 'Site 33' ];
  filteredOptions: Observable<string[]>;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.siteList.filter(option => option.toLowerCase().includes(filterValue));
  }

  openDialog() {
    this.dialog.open(AddNewAutomaticReportDialog);
  }

  edit(){
    console.log("Edit Automatic Report");
  }

  delete(){
    console.log("Delete Automatic Report");
  }
}

@Component({
  selector: 'add-new-automatic-report-dialog.html',
  templateUrl: 'add-new-automatic-report-dialog.html',
  styleUrls: ['add-new-automatic-report.scss']
})
export class AddNewAutomaticReportDialog {

   openDialog() {
    console.log("adding new reports");
  }
}